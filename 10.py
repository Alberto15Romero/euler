from isPrime import isPrime

sum = 0
for i in range(1, 2000000, 2):
    if isPrime(i):
        sum += i

print(sum)