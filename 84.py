def monop(finish_order=6,games_order=3):
    
    finish = 10**finish_order
    games = 10**games_order
    
    import random
    from random import shuffle
    import heapq
    
    squares = []
    index = []

    for i in range(0, 40):
        index.append(i)
    
    while len(squares) < 40:
        squares.append(0)
    
    # roll values are values from a six by six grid for all dice rolls
    rollvalues = [2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8]
    #rollvalues = [2,3,4,5,6,7,3,4,5,6,7,8,4,5,6,7,8,9,5,6,7,8,9,10,6,7,8,9,10,11,7,8,9,10,11,12]
    
    games_finished = 0
    
    while games_finished < games:
        if games_finished % 100 == 0:
            print(games_finished)

        master_chest = [0,10,40,40,40,40,40,40,40,40,40,40,40,40,40,40]
        chest = [i for i in master_chest]
        shuffle(chest)
        
        master_chance = [0,10,11,24,39,5,'R','R','U','B',40,40,40,40,40,40]
        chance = [i for i in master_chance]
        shuffle(chance)
        
        doubles = 0
        
        position = 0
        
        gos = 0
        
        while gos < finish:
            
            diceroll = int(16*random.random())
            
            if diceroll in [0,7,14,21,28,35]:    # these are the dice index values for double rolls
                doubles += 1
            else:
                doubles = 0
            if doubles >= 3:
                position = 10
            else:
                
                position = (position + rollvalues[diceroll])%40
                
                if position in [7,22,36]:  # Chance
                    chance_card = chance.pop(0)
                    if len(chance) == 0:
                        chance = [i for i in master_chance]
                        shuffle(chance)
                    if chance_card != 40:
                        
                        if isinstance(chance_card,int):
                            position = chance_card
                        elif chance_card == 'U':
                            while position not in [12,28]:
                                position = (position + 1)%40
                        elif chance_card == 'R':
                            while position not in [5,15,25,35]:
                                position = (position + 1)%40
                        elif chance_card == 'B':
                            position = position - 3
                            
                elif position in [2,17,33]:  # Community Chest
                    chest_card = chest.pop(0)
                    if len(chest) == 0:
                        chest = [i for i in master_chest]
                        shuffle(chest)
                    if chest_card != 40:
                        position = chest_card
                        
                if position == 30: # Go to jail
                    position = 10
                    
                    
            squares.insert(position,(squares.pop(position)+1))
            
            gos += 1
        
        games_finished += 1
    
    
    return heapq.nlargest(3, zip(squares, index))

print(monop(4,2))