def isTrivial(num1, num2):
    if num1 % 10 == 0 and num2 % 10 == 0:
        return True
    
def canCancel(num1, num2):
    if set(str(num1)).intersection(set(str(num2))) != set():
        return True
    return False
    
totalNum = 1
totalDen = 1
for i in range(10, 100):
    for j in range(10, 100):
        if not isTrivial(i, j) and canCancel(i, j) and i > j:
            div = j/i
            if str(i)[0] == str(j)[0]:
                iRes = int(str(i)[1])
                jRes = int(str(j)[1])
            elif str(i)[1] == str(j)[0]:
                iRes = int(str(i)[0])
                jRes = int(str(j)[1])
            elif str(i)[0] == str(j)[1]:
                iRes = int(str(i)[1])
                jRes = int(str(j)[0])
            elif str(i)[1] == str(j)[1]:
                iRes = int(str(i)[0])
                jRes = int(str(j)[0])
            
            if iRes != 0 and div == jRes/iRes:
                totalNum *= jRes
                totalDen *= iRes
n = 0
for i in range(1, min(totalNum, totalDen) + 1):
    if totalNum % i == 0 and totalDen % i == 0:
        n = i
      
print(int(totalDen/n))
  