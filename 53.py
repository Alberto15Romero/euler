import math
res = 0
for n in range(0, 101):
    for r in range(0, n+1):
        comb = math.factorial(n)/(math.factorial(r)*math.factorial(n-r))
        if (comb > 1000000):
            res += 1
print(res)