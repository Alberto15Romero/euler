def sumDigits(n):
    total = 0
    n = str(n)
    for k in range(0, len(n)):
        total += int(n[k])
    return total

res = 0

for i in range(1, 101):
    for j in range(1, 101):
        res = max(sumDigits(i**j), res)

print(res)