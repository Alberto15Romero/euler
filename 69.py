from utils import primesUnderN

maximum = 0
primes = primesUnderN(1000001)
for n in range(4, 1000001):
    if n % 1000 == 0:
        print(n)

    phi = n
    for prime in primes:
        if prime > n:
            break

        phi *= (1-(1/prime))    


    nOverPhi = n/phi
    if nOverPhi > maximum:
        maximum = nOverPhi
        result = n

print(result)