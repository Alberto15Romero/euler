sum = 0
def fact(n):
    if n == 1:
        return 1
    else:
        return n*fact(n-1)

num = str(fact(100))

for i in range(0, len(num)):
    sum += int(num[i])
print(sum)