items = open('data/59.txt', 'r').read().split(',')

dic1 = {}
dic2 = {}
dic3 = {}
letters = 0
extract = ''
#get dict of letter frequency
for i in range(len(items)):
    if i % 3 == 0:
        if dic1.get(items[i]) == None:
            dic1[items[i]] = 1
        else: 
            dic1[items[i]] += 1

    if i % 3 == 1:
        if dic2.get(items[i]) == None:
            dic2[items[i]] = 1
        else: 
            dic2[items[i]] += 1

    if i % 3 == 2:  
        if dic3.get(items[i]) == None:
            dic3[items[i]] = 1
        else: 
            dic3[items[i]] += 1


#manually check the letter frequency, assuming ' ', 'e', 't' atc are the most frequent letters in the english language, tial and error to get the password 'exp'.

#xor each letter to its corresponding value and get an extract
for i in range(len(items)):
    if i % 3 == 0:
        letters += int(items[i])^101
        extract += chr(int(items[i])^101)

    if i % 3 == 1:
        letters += int(items[i])^120
        extract += chr(int(items[i])^120)

    if i % 3 == 2:  
        letters += int(items[i])^112
        extract += chr(int(items[i])^112)

print(letters)
print(extract)