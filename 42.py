total = 0
for line in open('data/data_problem42.txt', 'r+').read().split(','):
    total += 1 if (-0.5 + ((0.25) - (2*-sum(list(map(lambda letter: ord(letter) - 64, line)))))**0.5) % 1 == 0.0 else 0
print(total)