def palin(n):
    return all(( n % 10 == n // 100000 % 10, n // 10 % 10 == n // 10000 % 10,  n // 100 % 10 == n // 1000 % 10))
      
### Main.

lst = []
for n in range(990, 100, -11): # only multiples of 11.
    for m in range(999, 100, -1):
        k = n * m
        if palin(k):
            lst.append(k)
            break

print(max(lst))