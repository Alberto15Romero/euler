from isPrime import isPrime

counter = 0
a = 2
while True:
    if isPrime(a):
        counter += 1
    if counter == 10001:
        print(a)
        break
    a += 1