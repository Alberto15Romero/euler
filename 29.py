import math
res = []
for a in range(2, 101):
    for b in range(2,101):
        ans = a**b
        if ans not in res:
            res.append(ans)

res.sort()
print(len(res))
