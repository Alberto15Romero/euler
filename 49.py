from utils import isPrime

def isPermutation(i, j):
    a = sorted(list(str(i)))
    b = sorted(list(str(j)))

    return a == b

for i in range(1000, 10000):
    for j in range(1000, 10000):
        diff = abs(i-j)
        k = j + diff
        if len(str(k)) == 4 and i != j and i != k and j != k and isPermutation(i, j) and isPermutation(i, k) and isPrime(i) and isPrime(j) and isPrime(k):
            res = str(i) + str(j) + str(k)
            if res != '148748178147':
                print(res)
                exit()