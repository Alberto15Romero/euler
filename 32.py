def isUseless(num):
    num = list(str(num))
    if '0' in num:
        return True
    for i in range(len(num)):
        for j in range(i+1, len(num)):
            if num[i] == num[j]:
                return True
    return False

resList = []
for i in range(1, 9999):
    if isUseless(i):
        continue
    for j in range(1, 999):
        if isUseless(j):
            continue
        mult = i * j
        if isUseless(mult):
            continue

        join = str(i) + str(j) + str(mult)
        if len(join) != 9:
            continue
        
        if not isUseless(join) and mult not in resList:
            resList.append(mult)
            print(i, j, mult)

print(sum(resList))