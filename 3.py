from math import sqrt, floor

def largest_prime_factor(number):
    primes = []
    for i in range(2, floor(sqrt(number))):
        if is_multiple_of_discovered_prime(i, primes):
            continue
        if number % i == 0:
            primes.append(i)
        while number % i == 0:
            number = number / i
    return max(primes)


def is_multiple_of_discovered_prime(i, primes):
    for prime_factor in primes:
        if i % prime_factor == 0:
            return True
    return False

print(largest_prime_factor(600851475143))