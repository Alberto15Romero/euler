def fib(n):
    if n == 1 :
        return 1
    if n == 2 :
        return 1
    else:
        return fib(n-1)+fib(n-2)

res = 0
i = 1
while fib(i) < 4000000:

    if fib(i) % 2 == 0:
        res += fib(i)
    i += 1

print(res)