import re

f = open('data/data_problem11.txt', 'r+')
r = f.read()

grid = []
max = 0
prod = 0

for k in r.split(' '):
    if k[0] == '0':
        k = k[1:]
    k = int(k)
    grid.append(k)

#Recorrer horizontal
for i in range(0, len(grid)):
    if i % 20 == 17:
        pass
    elif i % 20 == 18:
        pass
    elif i % 20 == 19:
        pass
    else:
        prod = grid[i]*grid[i+1]*grid[i+2]*grid[i+3]
    if prod > max:
        max = prod

#Recorrer vertical
for i in range(0, len(grid)-60):
    prod = grid[i]*grid[i+20]*grid[i+40]*grid[i+60]
    if prod > max:
        max = prod


#Recorrer diagonales principales
for i in range(0, len(grid)-63):
    if i % 20 == 17:
        pass
    elif i % 20 == 18:
        pass
    elif i % 20 == 19:
        pass
    else:
        
        prod = grid[i]*grid[i+21]*grid[i+42]*grid[i+63]
        if prod > max:
            max = prod
        
#Recorrer diagonales secundarias
for i in range(0, len(grid)-60):
    if i % 20 == 0:
        pass
    elif i % 20 == 1:
        pass
    elif i % 20 == 2:
        pass
    else:
        
        prod = grid[i]*grid[i+19]*grid[i+38]*grid[i+57]
        if prod > max:
            max = prod
print(max)
