import time
FACE_CARDS = {'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}
Values = {'High Card': 1, 'One Pair' : 2, 'Two Pairs' : 3, 'Three of a Kind' : 4, 'Straight' : 5, 'Flush': 6, 'Fullhouse': 7, 'Four of a Kind' : 8, 'Straight Flush': 9, 'Royal Flush' : 10}
def to_number(card):
    if card[0].isnumeric():
        return int(card[0])
    else:
        return FACE_CARDS[card[0]]

def hand(s):
    dic = {}
    ha = []
    ha.append(s[0:2])
    ha.append(s[3:5])
    ha.append(s[6:8])
    ha.append(s[9:11])
    ha.append(s[12:14])
    ha.sort(key=to_number)
    
    #Create a dictionary where every card of the hand is counted
    for i in range(0, len(ha)):
        dic.setdefault(to_number(ha[i][0]), 0)
        dic[to_number(ha[i][0])] += 1

    three = False
    two = False
    pair1 = False
    pair2 = False
    allOne = True
    count = 0
    res = ''
    res1 = ''
    res2 = ''
    for key in dic.keys():
        count += 1

        #High Card
        if dic.get(key) != 1:
            allOne = False
        
        #Two Pairs
        if dic.get(key) == 2 and not pair1:
            pair1 = True
            if key < 10:
                res1 = '0'
                res1 += str(key)
            else:
                res1 = str(key)
            localCount = count
        if pair1 and dic.get(key) == 2 and localCount != count:
            pair2 = True
            if key < 10:
                res2 = '0'
                res2 += str(key)
            else:
                res2 = str(key)
        if pair1 and pair2 and len(dic) == count:
            res = 'Two Pairs'

        #One Pair
        if ((pair1 and not pair2) or (pair2 and not pair1)) and count==len(dic):
            if pair1:
                if key < 10:
                    res1 = '0'
                    res1 += str(key)
            if pair2:
                if key < 10:
                    res2 = '0'
                    res2 += str(key)
            res = 'One Pair'

        #Four of a Kind
        if dic.get(key) == 4:
            if key < 10:
                res1 = '0'
            res1 += str(key)
            res = 'Four of a Kind'
        
        #Three of a Kind
        if dic.get(key) == 3 :
            three = True
            if key < 10:
                res1 = '0'
                res1 += str(key)
            else:
                res1 = str(key)
        if three and count == len(dic):
            res = 'Three of a Kind'

        #Fullhouse
        if dic.get(key) == 3:
            three = True
        if dic.get(key) == 2:
            two = True
        if three and two and len(dic) == count:
            res = 'Fullhouse'

        if allOne == True:
            if key < 10:
                res1 = '0'
                res1 += str(key)
            else:
                res1 = str(key)
            res =  'High Card'

    #Straight
    if to_number(ha[0][0]) == to_number(ha[1][0])-1 and to_number(ha[0][0]) == to_number(ha[2][0])-2 and to_number(ha[0][0]) == to_number(ha[3][0])-3 and to_number(ha[0][0]) == to_number(ha[4][0])-4:
        if key < 10:
                res1 = '0'
                res1 += str(key)
        else:
            res1 = str(key)
        res = 'Straight'

    #Flush
    if ha[0][1] == ha[1][1] and ha[1][1] == ha[2][1] and ha[2][1] ==  ha[3][1] and ha[3][1] == ha[4][1]:
        if key < 10:
                res1 = '0'
                res1 += str(key)
        else:
            res1 = str(key)
        res =  'Flush'

    #Stright Flush
    if to_number(ha[0][0]) == to_number(ha[1][0])-1 and to_number(ha[0][0]) == to_number(ha[2][0])-2 and to_number(ha[0][0]) == to_number(ha[3][0])-3 and to_number(ha[0][0]) == to_number(ha[4][0])-4 and \
    ha[0][1] == ha[1][1] and ha[1][1] == ha[2][1] and ha[2][1] ==  ha[3][1] and ha[3][1] == ha[4][1]:
        res = 'Straight Flush'

    #Royal Flush
    if ha[0][0] == 'T' and ha[1][0] == 'J' and ha[2][0] == 'Q' and ha[3][0] == 'K' and ha[4][0] == 'A'and ha[0][1] == ha[1][1] and ha[1][1] == ha[2][1] and ha[2][1] ==  ha[3][1] and ha[3][1] == ha[4][1]:
        res =  'Royal Flush'

    if res == 'Two Pairs':
        if res1 > res2:
            res += res1 + res2
        else:
            res += res2 + res1
    else:
        res += res1
    return res
    

if __name__ == "__main__":
    f1 = open('data_problem54.txt', 'r+').read()
    total = 0
    for h in f1.split('\n'):
        a = hand(h[0:15])
        b = hand(h[15:])

        if 'Two' in a:
            player1 = a[:-4]
            value1 = a[-4:]
            if value1[0] == '0':
                    value11 = int(value1[1])
            else:
                value11 = int(value1[0:3])
            if value1[2] == '0':
                    value12 = int(value1[3])
            else:
                value12 = int(value1[-2:])

        if 'Two' in b:
            player2 = b[:-4]
            value2 = b[-4:]
            if value2[0] == '0':
                    value21 = int(value2[1])
            else:
                value21 = int(value2[2:4])
            if value2[2] == '0':
                    value22 = int(value2[-2:])
            else:
                value12 = value1

        if 'Two' not in a:
            player1 = a[:-2]
            value1 = a[-2:]
            if value1[0] == '0':
                    value1 = int(value1[1])
            else:
                value1 = int(value1)

        if 'Two' not in b:
            player2 = b[:-2]
            value2 = b[-2:]
            if value2[0] == '0':
                    value2 = int(value2[1])
            else:
                value2 = int(value2)

        if Values.get(player1) > Values.get(player2):
            total += 1
        elif Values.get(player1) == Values.get(player2):
            if value1 > value2:
                total+= 1
    print(total-2)