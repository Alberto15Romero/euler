from utils import isPrime, uniqueDivisorsOfN

total = 0
for i in range(1, 100000000): # it takes some time (maybe an hour or two) but it gets there 
    divisors = uniqueDivisorsOfN(i)
    divisors.append(i)
    divisors.append(1)

    toCheck = list(map(lambda x: isPrime(x+(i/x)), divisors))
    if toCheck != [] and all(toCheck):
        total += i

print(total)