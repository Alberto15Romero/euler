import math
from decimal import *

def largestPrimeFactor(number):
    primes = []
    for i in range(2, floor(sqrt(number))):
        if is_multiple_of_discovered_prime(i, primes):
            continue
        if number % i == 0:
            primes.append(i)
        while number % i == 0:
            number = number / i
    return max(primes)

def isLeapYear(a):
    if a%4 == 0: 
        if a%100 == 0:
            if a%400 == 0:
                return True
            else:
                return False
        return True
    else:
        return False

def decimalToBinary(num): 
    return str(bin(num))[2:]

def isPalindrome(string): 
    return string == string[::-1] 

def isCircularPrime(num):
    a = num
    while True:
        a = nextRotation(a)
        if a == num:
            break
        if not isPrime(a):
            return False
    return True

def isPentNum(n):
    c = -n
    b = -0.5
    a = 1.5
    solution = ((b * -1) + ((b**2) - (4*a*c))**0.5)/(a*2)
    return solution % 1 == 0

def collatzChainLength(n):
    count = 2
    while(n != 1):
    
        if n % 2 == 0:
            n/=2
        else:
            n = n*3 + 1
        count += 1
    return count

def isNthRoot(num, root):
    number = round(num**(1/root), 10)
    return math.floor(number) == int(number)

def isPrime(n):
    if n == 2: return True
    if n % 2 == 0:
        return False
    d = 3
    while d * d <= n:
        if n % d == 0:
            return False
        d += 2
    return True

def numberOfDivisors(n):
    limit = math.floor(math.sqrt(n))
    d = 1 ## contamos que 1 es divisor
 
    for i in range(2, limit):  ##y empezamos desde 2
        if n % i == 0:
            ## Por cada divisor debajo de la raíz
            ## hay un divisor encima de la raíz
            d += 2
    

    ## Corregir si es un cuadrado perfecto
    if limit*limit == n:
        d -= 1
    
    ## Quitar lo siguiente si sólo queremos los divisores propios.
    ## Si es distinto de uno, tenemos un divisor más
    if n != 1:
        d += 1 ## Sí mismo
    return d

def uniqueDivisorsOfN(n):
    divisors = []
    limit = math.floor(math.sqrt(n))

    for i in range(2, limit+1):  ##y empezamos desde 2
        if n % i == 0:
            ## Por cada divisor debajo de la raíz hay un divisor encima de la raíz
            divisors.append(i)
            divisors.append(int(n/i))

    ## Corregir si es un cuadrado perfecto
    if limit*limit == n and n != 1:
        divisors.pop(len(divisors)-1)

    return sorted(divisors)

def divisorsOfN(n):
    divisors = []
    limit = math.floor(math.sqrt(n))

    for i in range(2, limit+1):  ##y empezamos desde 2
        if n % i == 0:
            ## Por cada divisor debajo de la raíz hay un divisor encima de la raíz
            divisors.append(i)
            divisors.append(int(n/i))

    return sorted(divisors)

def isPandigital(num, includeZero):
    num = list(str(num))
    if '0' in num and not includeZero:
        return False

    for digit in num:
        if int(digit) > len(num):
            return False

    for i in range(len(num)):
        for j in range(i+1, len(num)):
            if num[i] == num[j]:
                return False
    return True

def lowestPrimeFactor(n):
    divisorList = divisorsOfN(n)
    for divisor in divisorList:
        if isPrime(divisor):
            return divisor

def primeDecomposition(n):
    num = n
    res = []
    while not isPrime(num):
        div = lowestPrimeFactor(num)
        num /= div
        res.append(div)
    res.append(int(num))
    return sorted(res)

def primesUnderN(n):
    primes = []
    for i in range(2, n + 1):
        if isPrime(i):
            primes.append(i)
    
    return primes

def primeGenerator(n, m):
    primes = []
    for i in range(n, m + 1):
        if isPrime(i):
            primes.append(i)
    return primes