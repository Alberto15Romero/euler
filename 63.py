import math

def logi(a, b):
    return math.log(a, b)

total = 1 #ya contamos que 1**1 produce un número de un dígito y por ello se excuye la base 1
R = 10

'''
b**x = a * R**(x-1), 1 < a < R, 1 < b < R
para que b**x tenga x dígitos en base R (base representa si es decimal, binario, octal, hexadecimal etc...)
resolviendo para x, se llega a:
x = (logi(a, b) - logi(R, b)) / (1 - logi(R, b))
como el logaritmo es lineal, podemos tomar todos los enteros entre x(a = 1) y x(a = R), pero 
x(a = R) = 0, y por ello a2 = 1 para no contar el 0.
'''

for b in range(2, R):

    a2 = 1
    a1 = (logi(1, b) - logi(R, b)) / (1 - logi(R, b))

    a2 = math.floor(a2)
    a1 = math.ceil(a1)

    for i in range(a2, a1):
        total += 1
print(total)