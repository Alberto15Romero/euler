count = 0
years = []
for i in range(1901,2001):
    years.append(i)
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]

CurrentWeekDay = 1

def isLeap(a):
    if a%4 == 0: 
        if a%100 == 0:
            if a%400 == 0:
                return True
            else:
                return False
        return True
    else:
        return False

for year in years:
    for month in months:
        for day in days:
            if(month == 'Feb' and day == 28 and isLeap(year)):
                CurrentWeekDay = (CurrentWeekDay + 1) % 7
            if(day == 31 and (month == 'Apr' or month == 'Jun' or month == 'Sep' or month == 'Nov')):
                CurrentWeekDay = (CurrentWeekDay - 1) % 7
            if(month == 'Feb' and day == 28 and not isLeap(year)):
                CurrentWeekDay = (CurrentWeekDay - 3) % 7
            if(CurrentWeekDay == 6 and day == 1):
                count += 1
            CurrentWeekDay = (CurrentWeekDay + 1) % 7
print(count)