
letter = [(0,0),(1,3,), (2,3), (3, 5), (4,4), (5,4), (6,3), (7,5), (8,5), (9,4), (10,3), (11,6), (12, 6), (13, 8),
 (14,8), (15,7), (16,7), (17,9), (18,8), (19,8), (20,6), (30, 6), (40,5), (50,5), (60,5), (70,7), (80, 6), (90, 6), (100, 7)]

letDic = {}
for le in letter:
    letDic.setdefault(le[0], le[1])

def lower100(n):
    total = 0
    if n <= 20:
        total += letDic[n]
    elif n < 100 and n >= 20:
        s = n
        if s % 10 != 0:
            for i in range(0, 9):
                s -= 1
                if s % 10 == 0:
                    break
        total += letDic[s] + letDic[n%10]
    return total

def numToWords(n):
    total = 0
    if n < 1000 and n >= 100:
        s = n
        if s % 100 != 0:
            for i in range(0, 99):
                s -= 1
                if s % 100 == 0:
                    break
            s /= 100
            s = int(s)
            total = letDic[s] + letDic[100] + lower100(n-(s*100)) +3
        else:
            total = letDic[n/100] + letDic[100]
        return total
    else:
        return lower100(n)

sum = 0
for i in range(1, 1000):
    sum += numToWords(i)
print(sum+11)