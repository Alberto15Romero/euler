from utils import isPrime, isPandigital

for i in range(87654321, 0, -1):
    if isPandigital(i, False) and isPrime(i):
        print(i)
        break