from utils import isPrime

def isTwiceSquare(num):
    i = 0
    while 2 * i**2 < num:
        i += 1
    return (2 * i**2) == num

primes = [2]

num = 3
flag = True
while flag:
    flag = False
    if isPrime(num):
        primes.append(num)
        flag = True
    else:
        for i in range(len(primes) - 1, -1, -1):
            diff = num-primes[i]
            if isTwiceSquare(diff):
                flag = True
                break
    num += 2
print(num - 2)