import numpy as np

totals = np.zeros((1001,), dtype=int)
print('This may take some time, but it will finish in about 5 mins')
for i in range(0, 1001):
    for j in range(0, 1001):
        for k in range(0, 1001):
            triplet = [i, j, k]
            sumTriplets = sum(triplet)
            sortedTriplet = sorted(triplet)
            if sortedTriplet[0]**2 + sortedTriplet[1]**2 == sortedTriplet[2]**2 and sumTriplets <= 1000:
                totals[sumTriplets] += 1

print(np.argmax(totals))