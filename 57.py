from fractions import Fraction

counter = 0
value = Fraction(1,2)
iterator = iter(range(1, 1001))
for i in iterator:
    value = Fraction(1, Fraction(2,1) + Fraction(value))
    res =  Fraction(1, 1) + Fraction(value, 1)
    if len(str(res.numerator)) > len(str(res.denominator)):
        counter += 1
print(counter)