def large(a, b, c, m):
    return (c * pow(a, b, m) + 1) % m

print(large(2, 7830457, 28433, 10**10))
