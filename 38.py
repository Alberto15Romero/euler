def isPandigital(num):
    num = list(str(num))
    if '0' in num:
        return False

    for digit in num:
        if int(digit) > len(num):
            return False

    for i in range(len(num)):
        for j in range(i+1, len(num)):
            if num[i] == num[j]:
                return False
    return True

for j in range(1234, 10000):
    number = ''
    for i in range(1, 10):
        number += str(j * i)

        if len(number) == 9 and isPandigital(int(number)):
            print(j, number)
