import math
from isPrime import isPrime

def isCircularPrime(num):
    a = num
    while True:
        a = nextRotation(a)
        if a == num:
            break
        if not isPrime(a):
            return False
    return True

def nextRotation(num):
    num = str(num)
    first_digit = num[0]
    return int(str(num[1:]) + first_digit)

res = 13
result = [2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97]

for i in range(101, 1000001):
    if '0' not in str(i) and '2' not in str(i) and '4' not in str(i) and '6' not in str(i) and '8' not in str(i):
        if isPrime(i):
            if isCircularPrime(i):
                res += 1
                result.append(i)
    
print(res)