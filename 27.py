from isPrime import isPrime

globalA = 0
globalB = 0
largestSequence = 0

for a in range(-999, 1000):
    for b in range(-999, 1000):
        n = 0
        while True:
            if not isPrime((n**2) + (a*n) + b):
                if n > largestSequence:
                    globalA = a
                    globalB = b
                    largestSequence = n
                break
            else:
                n += 1

print(globalA*globalB)