from isPrime import isPrime

def isLeftTruncatable(n):
    word = str(n)
    while len(word) >= 1:
        if not isPrime(int(word)):
            return False
        else:
            word = word[1:]
    return True

def isRightTruncatable(n):
    word = str(n)
    while len(word) >= 1:
        if not isPrime(int(word)):
            return False
        else:
            word = word[:len(word)-1]
    return True


counter = 0
res = 0
i = 10
while counter != 11:
    if isLeftTruncatable(str(i)) and isRightTruncatable(str(i)):

        res += i
        counter += 1
    i += 1

print(res)