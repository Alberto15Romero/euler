from utils import isPrime

primes = []
ans = 0
res = 0

for i in range(1, 1000000):

    if i % 1000 == 0:
        print(i)

    if isPrime(i):
        primes.append(i)
        counter1 = len(primes) - 2
        counter2 = len(primes) - 3
        sumInter = primes[counter1]
        sumPrimes = 1

        while counter1 > 1:
            sumInter += primes[counter2]

            if sumInter > i:
                counter1 -= 1
                counter2 = counter1 - 1
                sumInter = primes[counter1]
                sumPrimes = 1

            elif sumInter == i:
                sumPrimes += 1
                if sumPrimes >= ans:
                    ans = sumPrimes
                    res = i
            else:
                counter2 -= 1
                sumPrimes += 1

print(res, ans)