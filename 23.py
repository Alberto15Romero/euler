from divisors import divisors

def sumOfDivisors(num):
    return sum(divisors(num))

abundants = []

for i in range(1, 28124):
    if sumOfDivisors(i) > i:
        abundants.append(i)

def canBeMadeBy2Abundants(n, list):
    for i, number in enumerate(list[:-1]):
        complementary = n - number
        if complementary in list[i:]:
            return True
            break
    else:
        return False

print('This can take some time but it will finish, dont worry')

total = 0
for k in range(0, 28123):
    if not canBeMadeBy2Abundants(k, abundants):
            total += k
print(total)