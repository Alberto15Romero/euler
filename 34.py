import math
counter = 0
i = 10
sum = 0
limit = math.factorial(9)*9
while i < limit:
    factSumDig = 0
    for j in range(0, len(str(i))):
        factSumDig += math.factorial(int(str(i)[j]))
    if factSumDig == i:
        sum += i
    i += 1
print(sum)