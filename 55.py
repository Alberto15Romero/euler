def reverseNum(x):
    return int(str(x)[::-1])

def isPalindrome(x):
    return str(x)[::-1] == str(x)

res = 10000
for i in range(1,10001):
    check = i
    for j in range(1, 51):
        if isPalindrome(check + reverseNum(check)):
            res -= 1
            break
        else:
            check = check + reverseNum(check)
print(res)