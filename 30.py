def isSumOfPowers(n):
    numList = list(str(n))
    sumListPow = 0
    for i in numList:
        sumListPow += int(i)**5
    
    return sumListPow == n

total = 0
i = 2
a = 0
while a < 10000000:
    if isSumOfPowers(i):
        total += i
        a = 0
    else:
        a += 1
    i += 1
print(total)