array = range(1, 21)
def conditionApplies(n):
    for i in range(1, len(array)):
        if n % array[i] != 0:
            return False
    return True


a = 21
while True:
    if a % 1000000 == 0:
        print(a)
    if conditionApplies(a):
        print(a)
        break
    a += 1