def SumSquareDigits(n):
    sum = 0
    for digit in str(n):
        sum += int(digit)**2

    return sum

total = 0
for i in range(1, 10000000):
    if i % 10000 == 0:
        print(i)

    num = i

    while num != 1 and num != 89:
        num = SumSquareDigits(num)
        if num == 89:
            total += 1

print(total + 1)