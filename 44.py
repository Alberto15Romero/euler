def isPentNum(n):
    c = -n
    b = -0.5
    a = 1.5
    solution = ((b * -1) + ((b**2) - (4*a*c))**0.5)/(a*2)
    return solution % 1 == 0

n = 1
minimum = float('inf')
pentNums = {}
while True:
    lastPentNum = int(((3*n**2)-n)/2)
    pentNums[n] = lastPentNum
    for i in range(len(pentNums), 0, -1):
        D = lastPentNum - pentNums[i]
        S = lastPentNum + pentNums[i]
        if isPentNum(int(D)) and isPentNum(int(S)):
            if minimum > D:
                minimum = D
                print(D)
                exit()
    n += 1