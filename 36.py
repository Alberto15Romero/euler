def decimalToBinary(num): 
    return str(bin(num))[2:]

def isPalindrome(string): 
    return string == string[::-1] 

res = 0
for i in range(1, 1000000):
    if isPalindrome(str(i)) and isPalindrome(decimalToBinary(i)):
        res += i

print(res)