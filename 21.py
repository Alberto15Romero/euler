from divisors import divisors
def sumOfDivisors(n):
    return sum(divisors(n))

num = [0]*10001
num[0] = None
for i in range(1, 10001):
    num[i] = sumOfDivisors(i)

total = 0
for i in range(1,10001):
    x = num[i]
    if x<len(num)-1:
        y = num[x]
    else:
        y = -1 
    if y == i and i != x:
        total += i + x

print(int(total/2))

