from utils import isPrime, primeDecomposition

def multiplyEqualFactors(factores):
    res = []
    for value in set(factores):
        mult = factores.count(value)
        res.append(value**mult)
    return sorted(res)

n = 646
while True:

    val1 = multiplyEqualFactors(primeDecomposition(n))
    val2 = multiplyEqualFactors(primeDecomposition(n+1))
    val3 = multiplyEqualFactors(primeDecomposition(n+2))
    val4 = multiplyEqualFactors(primeDecomposition(n+3))

    if val1 != val2 != val3 != val4:
        if len(val1) == 4 and len(val2) == 4 and len(val3) == 4 and len(val4) == 4:
            print(n)
            break
    
    n += 1