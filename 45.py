triangular = []
pentagonal = []
hexagonal = []

def Triangular(n):
    return n*(n+1)/2

def Pentagonal(n):
    return n*((n*3)-1)/2

def Hexagonal(n):
    return n*((n*2)-1)

for i in range(0, 55386):
    triangular.append(Triangular(i))
    pentagonal.append(Pentagonal(i))
    hexagonal.append(Hexagonal(i))

print(sorted(list(set(triangular) & set(pentagonal) & set(hexagonal))))